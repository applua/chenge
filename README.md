# EVE多功能信息查询器

#### 介绍
该软件旨在提供一个EVE online游戏的信息查询工具。
![多功能信息查询器](https://images.gitee.com/uploads/images/2021/0111/175023_5047bc61_13924.png "多功能信息查询器.PNG")

#### 软件架构
软件采用主窗体、子窗体设计，通过工具栏切换相关模块显示。
物品数据采用sqlite本地数据库，价格采用[国服市场中心数据](https://www.ceve-market.org/index/) 。
游戏资料收集于网上,如[EVE WIKI](htttp://www.evewiki.vip)、 [攻略百科](http://evepc.baike.163.com)等。
####  编译环境

1 编程语言采用C# winform，编程工具采用VS2022。  
2 数据库使用sqlite插件。

#### 使用说明

1.  Windows系统下直接运行change.exe。
2.  系统运行时部分内容需要网络支持。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request 或者 直接提交轻量级PR。

#### 其他说明
1 体验版  需要在发行标签中下载   
2 由于网络资源的变化，部分资料失效，需更新   
3 想参与者强烈建议贡献代码   


