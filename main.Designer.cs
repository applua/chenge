﻿
namespace Change
{
    partial class main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssPlayers = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.ttssgVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssMarket = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbPrice = new System.Windows.Forms.ToolStripButton();
            this.tsbLP = new System.Windows.Forms.ToolStripButton();
            this.tsbBluePrint = new System.Windows.Forms.ToolStripButton();
            this.tsbRefinery = new System.Windows.Forms.ToolStripButton();
            this.tsbPlanet = new System.Windows.Forms.ToolStripButton();
            this.tsbItems = new System.Windows.Forms.ToolStripButton();
            this.tsbWormHole = new System.Windows.Forms.ToolStripButton();
            this.tsbAgents = new System.Windows.Forms.ToolStripButton();
            this.tsbTask = new System.Windows.Forms.ToolStripButton();
            this.tsbInfo = new System.Windows.Forms.ToolStripButton();
            this.tsbArcheology = new System.Windows.Forms.ToolStripButton();
            this.tsbAbyss = new System.Windows.Forms.ToolStripButton();
            this.tsbHistory = new System.Windows.Forms.ToolStripButton();
            this.tsbAbout = new System.Windows.Forms.ToolStripButton();
            this.subMain = new System.Windows.Forms.Panel();
            this.ttssgPlayers = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssPlayers,
            this.tssVersion,
            this.ttssgPlayers,
            this.ttssgVersion,
            this.tssTime,
            this.tssMarket});
            this.statusStrip1.Location = new System.Drawing.Point(0, 725);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 10, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1363, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssPlayers
            // 
            this.tssPlayers.Name = "tssPlayers";
            this.tssPlayers.Size = new System.Drawing.Size(212, 17);
            this.tssPlayers.Spring = true;
            this.tssPlayers.Text = "在线人数";
            // 
            // tssVersion
            // 
            this.tssVersion.Name = "tssVersion";
            this.tssVersion.Size = new System.Drawing.Size(212, 17);
            this.tssVersion.Spring = true;
            this.tssVersion.Text = "宁静服版本";
            // 
            // ttssgVersion
            // 
            this.ttssgVersion.Name = "ttssgVersion";
            this.ttssgVersion.Size = new System.Drawing.Size(212, 17);
            this.ttssgVersion.Spring = true;
            this.ttssgVersion.Text = "曙光服版本";
            this.ttssgVersion.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // tssTime
            // 
            this.tssTime.Name = "tssTime";
            this.tssTime.Size = new System.Drawing.Size(212, 17);
            this.tssTime.Spring = true;
            this.tssTime.Text = "开始时间";
            // 
            // tssMarket
            // 
            this.tssMarket.Name = "tssMarket";
            this.tssMarket.Size = new System.Drawing.Size(212, 17);
            this.tssMarket.Spring = true;
            this.tssMarket.Text = "国服市场中心状态";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(60, 60);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbPrice,
            this.tsbLP,
            this.tsbBluePrint,
            this.tsbRefinery,
            this.tsbPlanet,
            this.tsbItems,
            this.tsbWormHole,
            this.tsbAgents,
            this.tsbTask,
            this.tsbInfo,
            this.tsbArcheology,
            this.tsbAbyss,
            this.tsbHistory,
            this.tsbAbout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1363, 84);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbPrice
            // 
            this.tsbPrice.Image = ((System.Drawing.Image)(resources.GetObject("tsbPrice.Image")));
            this.tsbPrice.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPrice.Name = "tsbPrice";
            this.tsbPrice.Size = new System.Drawing.Size(64, 81);
            this.tsbPrice.Text = "价格中心";
            this.tsbPrice.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPrice.Click += new System.EventHandler(this.tsbPrice_Click);
            // 
            // tsbLP
            // 
            this.tsbLP.Image = ((System.Drawing.Image)(resources.GetObject("tsbLP.Image")));
            this.tsbLP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLP.Name = "tsbLP";
            this.tsbLP.Size = new System.Drawing.Size(72, 81);
            this.tsbLP.Text = "忠诚点兑换";
            this.tsbLP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbLP.Click += new System.EventHandler(this.tsbLP_Click);
            // 
            // tsbBluePrint
            // 
            this.tsbBluePrint.Image = ((System.Drawing.Image)(resources.GetObject("tsbBluePrint.Image")));
            this.tsbBluePrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbBluePrint.Name = "tsbBluePrint";
            this.tsbBluePrint.Size = new System.Drawing.Size(84, 81);
            this.tsbBluePrint.Text = "蓝图制造计算";
            this.tsbBluePrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbBluePrint.Click += new System.EventHandler(this.tsbBluePrint_Click);
            // 
            // tsbRefinery
            // 
            this.tsbRefinery.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefinery.Image")));
            this.tsbRefinery.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefinery.Name = "tsbRefinery";
            this.tsbRefinery.Size = new System.Drawing.Size(72, 81);
            this.tsbRefinery.Text = "提炼与再生";
            this.tsbRefinery.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbRefinery.Click += new System.EventHandler(this.tsbRefinery_Click);
            // 
            // tsbPlanet
            // 
            this.tsbPlanet.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlanet.Image")));
            this.tsbPlanet.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlanet.Name = "tsbPlanet";
            this.tsbPlanet.Size = new System.Drawing.Size(64, 81);
            this.tsbPlanet.Text = "行星开发";
            this.tsbPlanet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbPlanet.Click += new System.EventHandler(this.tsbPlanet_Click);
            // 
            // tsbItems
            // 
            this.tsbItems.Image = ((System.Drawing.Image)(resources.GetObject("tsbItems.Image")));
            this.tsbItems.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbItems.Name = "tsbItems";
            this.tsbItems.Size = new System.Drawing.Size(72, 81);
            this.tsbItems.Text = "物品数据库";
            this.tsbItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbItems.Click += new System.EventHandler(this.tsbItems_Click);
            // 
            // tsbWormHole
            // 
            this.tsbWormHole.Image = ((System.Drawing.Image)(resources.GetObject("tsbWormHole.Image")));
            this.tsbWormHole.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbWormHole.Name = "tsbWormHole";
            this.tsbWormHole.Size = new System.Drawing.Size(64, 81);
            this.tsbWormHole.Text = "虫洞查询";
            this.tsbWormHole.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbWormHole.Click += new System.EventHandler(this.tsbWormHole_Click);
            // 
            // tsbAgents
            // 
            this.tsbAgents.Image = ((System.Drawing.Image)(resources.GetObject("tsbAgents.Image")));
            this.tsbAgents.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAgents.Name = "tsbAgents";
            this.tsbAgents.Size = new System.Drawing.Size(72, 81);
            this.tsbAgents.Text = "代理人搜索";
            this.tsbAgents.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAgents.Click += new System.EventHandler(this.tsbAgents_Click);
            // 
            // tsbTask
            // 
            this.tsbTask.Image = ((System.Drawing.Image)(resources.GetObject("tsbTask.Image")));
            this.tsbTask.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbTask.Name = "tsbTask";
            this.tsbTask.Size = new System.Drawing.Size(64, 81);
            this.tsbTask.Text = "任务攻略";
            this.tsbTask.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbTask.Click += new System.EventHandler(this.tsbTask_Click);
            // 
            // tsbInfo
            // 
            this.tsbInfo.Image = ((System.Drawing.Image)(resources.GetObject("tsbInfo.Image")));
            this.tsbInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbInfo.Name = "tsbInfo";
            this.tsbInfo.Size = new System.Drawing.Size(84, 81);
            this.tsbInfo.Text = "异常死亡攻略";
            this.tsbInfo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbInfo.Click += new System.EventHandler(this.tsbInfo_Click);
            // 
            // tsbArcheology
            // 
            this.tsbArcheology.Image = ((System.Drawing.Image)(resources.GetObject("tsbArcheology.Image")));
            this.tsbArcheology.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbArcheology.Name = "tsbArcheology";
            this.tsbArcheology.Size = new System.Drawing.Size(64, 81);
            this.tsbArcheology.Text = "考古攻略";
            this.tsbArcheology.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbArcheology.Click += new System.EventHandler(this.tsbArcheology_Click);
            // 
            // tsbAbyss
            // 
            this.tsbAbyss.Image = ((System.Drawing.Image)(resources.GetObject("tsbAbyss.Image")));
            this.tsbAbyss.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAbyss.Name = "tsbAbyss";
            this.tsbAbyss.Size = new System.Drawing.Size(64, 81);
            this.tsbAbyss.Text = "深渊攻略";
            this.tsbAbyss.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAbyss.Click += new System.EventHandler(this.tsbAbyss_Click);
            // 
            // tsbHistory
            // 
            this.tsbHistory.Image = ((System.Drawing.Image)(resources.GetObject("tsbHistory.Image")));
            this.tsbHistory.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbHistory.Name = "tsbHistory";
            this.tsbHistory.Size = new System.Drawing.Size(84, 81);
            this.tsbHistory.Text = "新伊甸编年史";
            this.tsbHistory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbHistory.Click += new System.EventHandler(this.tsbHistory_Click);
            // 
            // tsbAbout
            // 
            this.tsbAbout.Image = ((System.Drawing.Image)(resources.GetObject("tsbAbout.Image")));
            this.tsbAbout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAbout.Name = "tsbAbout";
            this.tsbAbout.Size = new System.Drawing.Size(64, 81);
            this.tsbAbout.Text = "关于";
            this.tsbAbout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.tsbAbout.Click += new System.EventHandler(this.tsbAbout_Click);
            // 
            // subMain
            // 
            this.subMain.AutoScroll = true;
            this.subMain.AutoSize = true;
            this.subMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.subMain.Location = new System.Drawing.Point(0, 84);
            this.subMain.Margin = new System.Windows.Forms.Padding(2);
            this.subMain.Name = "subMain";
            this.subMain.Size = new System.Drawing.Size(1363, 641);
            this.subMain.TabIndex = 2;
            // 
            // ttssgPlayers
            // 
            this.ttssgPlayers.Name = "ttssgPlayers";
            this.ttssgPlayers.Size = new System.Drawing.Size(192, 17);
            this.ttssgPlayers.Spring = true;
            this.ttssgPlayers.Text = "曙光服在线人数";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1363, 747);
            this.Controls.Add(this.subMain);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EVE多功能信息查询器";
            this.Load += new System.EventHandler(this.main_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssPlayers;
        private System.Windows.Forms.ToolStripStatusLabel tssVersion;
        private System.Windows.Forms.ToolStripStatusLabel tssTime;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbPrice;
        private System.Windows.Forms.ToolStripButton tsbLP;
        private System.Windows.Forms.ToolStripButton tsbRefinery;
        private System.Windows.Forms.ToolStripButton tsbAgents;
        private System.Windows.Forms.ToolStripButton tsbPlanet;
        private System.Windows.Forms.ToolStripButton tsbItems;
        private System.Windows.Forms.ToolStripButton tsbBluePrint;
        private System.Windows.Forms.ToolStripButton tsbWormHole;
        private System.Windows.Forms.ToolStripButton tsbTask;
        private System.Windows.Forms.ToolStripButton tsbInfo;
        private System.Windows.Forms.ToolStripButton tsbHistory;
        private System.Windows.Forms.ToolStripButton tsbArcheology;
        private System.Windows.Forms.ToolStripButton tsbAbyss;
        private System.Windows.Forms.ToolStripButton tsbAbout;
        private System.Windows.Forms.Panel subMain;
        private System.Windows.Forms.ToolStripStatusLabel tssMarket;
        private System.Windows.Forms.ToolStripStatusLabel ttssgVersion;
        private System.Windows.Forms.ToolStripStatusLabel ttssgPlayers;
    }
}

