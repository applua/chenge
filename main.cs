﻿using Change.uc;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace Change
{
    public partial class main : Form
    {
        public ucPrice price;
        public ucLP lp;
        public ucRefinery refinery ;
        public ucBlueprint blueprint ;
        public ucAgents agents ;
        public ucPlanet planet ;
        public ucItems items ;
        public ucWormHole wormhole ;
        public ucTask task ;
        public ucInfo info ;
        public ucArcheology arch ;
        public ucAbyss abyss ;
        public ucHistory history ;
        public ucAbout about ;

        public main()
        {
            InitializeComponent();
            SetIE(IeVersion.强制ie10);
        }

        private void tsbPrice_Click(object sender, EventArgs e)
        {
                if (price == null)
                  price = new ucPrice();
   
                price.Dock = DockStyle.Fill;
                subMain.Controls.Clear();
                
                subMain.Controls.Add(price);          
           

        }
        private void initUC()
        {
            ucPrice price = new ucPrice();
            ucLP lp = new ucLP();
            ucRefinery refinery = new ucRefinery();
            ucBlueprint blueprint = new ucBlueprint();
            ucAgents agents = new ucAgents();
            ucPlanet planet = new ucPlanet();
            ucItems items = new ucItems();
            ucWormHole wormhole = new ucWormHole();
            ucTask task = new ucTask();
            ucInfo info = new ucInfo();
            ucArcheology arch = new ucArcheology();
            ucAbyss abyss = new ucAbyss();
            ucHistory history = new ucHistory();
            ucAbout about = new ucAbout();
        }

        private void tsbLP_Click(object sender, EventArgs e)
        {
            if (lp == null)
                lp = new ucLP();
            subMain.Controls.Clear();
            subMain.Controls.Add(lp);
        }

        private void tsbRefinery_Click(object sender, EventArgs e)
        {
            if (refinery == null)
                refinery = new ucRefinery();
            subMain.Controls.Clear();
            subMain.Controls.Add(refinery);
        }

        private void tsbBluePrint_Click(object sender, EventArgs e)
        {
            if (blueprint == null)
                blueprint = new ucBlueprint();
            subMain.Controls.Clear();
            subMain.Controls.Add(blueprint);
        }

        private void tsbAgents_Click(object sender, EventArgs e)
        {
            if (agents == null)
                agents = new ucAgents();
            subMain.Controls.Clear();
            subMain.Controls.Add(agents);

        }

        private void tsbPlanet_Click(object sender, EventArgs e)
        {
            if (planet == null)
                planet = new ucPlanet();
            subMain.Controls.Clear();
            subMain.Controls.Add(planet);
        }

        private void tsbItems_Click(object sender, EventArgs e)
        {
            if (items == null)
                items = new ucItems();
            subMain.Controls.Clear();
            subMain.Controls.Add(items);
        }

        private void tsbWormHole_Click(object sender, EventArgs e)
        {
            if (wormhole == null)
                wormhole = new ucWormHole();
            subMain.Controls.Clear();
            subMain.Controls.Add(wormhole);
        }

        private void tsbTask_Click(object sender, EventArgs e)
        {
            if (task == null)
                task = new ucTask();

            task.Dock = DockStyle.Fill;
            subMain.Controls.Clear();
            subMain.Controls.Add(task);

        }

        private void tsbInfo_Click(object sender, EventArgs e)
        {
            if (info == null)
                info = new ucInfo();
            info.Dock = DockStyle.Fill;
            subMain.Controls.Clear();
            subMain.Controls.Add(info);
        }

        private void tsbArcheology_Click(object sender, EventArgs e)
        {
            if (arch == null)
                arch = new ucArcheology();
            arch.Dock = DockStyle.Fill;
            subMain.Controls.Clear();
            subMain.Controls.Add(arch);
        }

        private void tsbAbyss_Click(object sender, EventArgs e)
        {
            if (abyss == null)
                abyss = new ucAbyss();
            abyss.Dock = DockStyle.Fill;
            subMain.Controls.Clear();
            subMain.Controls.Add(abyss);
        }

        private void tsbHistory_Click(object sender, EventArgs e)
        {
            if (history == null)
                history = new ucHistory();
            history.Dock = DockStyle.Fill;
            subMain.Controls.Clear();
            subMain.Controls.Add(history);
        }

        private void tsbAbout_Click(object sender, EventArgs e)
        {
            if (about == null)
                about = new ucAbout();
            subMain.Controls.Clear();
            subMain.Controls.Add(about);
        }

        private void main_Load(object sender, EventArgs e)
        {
            try
            {
                string url = "https://esi.evepc.163.com/latest/status/?datasource=serenity";
                string marketsql = "https://www.ceve-market.org/index/";
                string statuscode=null;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Proxy = null;
                request.KeepAlive = false;
                request.Method = "GET";
                request.ContentType = "application/json; charset=UTF-8";


                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream rs = response.GetResponseStream();
                StreamReader sr = new StreamReader(rs, Encoding.UTF8);
                string retString = sr.ReadToEnd();
                rs.Close();
                sr.Close();

                JObject json = (JObject)JsonConvert.DeserializeObject(retString);

                string players= (string)json["players"];
                string version=tssVersion.Text = (string)json["server_version"];
                DateTime dt =(DateTime) json["start_time"];

                tssPlayers.Text = "宁静服在线人数：" + players;
                tssVersion.Text = "宁静服版本:" + version;
                tssTime.Text = "开始时间："+dt.ToString("F");

                string urlin = "https://esi.evepc.163.com/latest/status/?datasource=infinity";


                request = (HttpWebRequest)WebRequest.Create(urlin);
                request.Proxy = null;
                request.KeepAlive = false;
                request.Method = "GET";
                request.ContentType = "application/json; charset=UTF-8";


                response = (HttpWebResponse)request.GetResponse();
                rs = response.GetResponseStream();
                sr = new StreamReader(rs, Encoding.UTF8);
                retString = sr.ReadToEnd();
                rs.Close();
                sr.Close();

                json = (JObject)JsonConvert.DeserializeObject(retString);

                players = (string)json["players"];
                version = tssVersion.Text = (string)json["server_version"];
                dt = (DateTime)json["start_time"];

                ttssgPlayers.Text = "曙光服在线人数：" + players;
                ttssgVersion.Text = "曙光服版本:" + version;
                tssTime.Text = "开始时间：" + dt.ToString("F");

                try
                {
                    HttpWebRequest req = (HttpWebRequest)WebRequest.CreateDefault(new Uri(marketsql));
                    req.Method = "HEAD";
                    req.Timeout = 1000;
                    HttpWebResponse res = (HttpWebResponse)req.GetResponse();
                    statuscode = res.StatusCode.ToString();
             
                }
                catch(WebException ex)
                {
                    throw ex;
                }
                tssMarket.Text = "国服市场中心状态：" + statuscode;
            }
            catch (Exception ex)
            {
                tssVersion.Text = "服务器连接出错";
            }


        }
        /// <summary>
        /// 定义IE版本的枚举
        /// </summary>
        private enum IeVersion
        {
            强制ie10,//10001 (0x2711) Internet Explorer 10。网页以IE 10的标准模式展现，页面!DOCTYPE无效
            标准ie10,//10000 (0x02710) Internet Explorer 10。在IE 10标准模式中按照网页上!DOCTYPE指令来显示网页。Internet Explorer 10 默认值。
            强制ie9,//9999 (0x270F) Windows Internet Explorer 9. 强制IE9显示，忽略!DOCTYPE指令
            标准ie9,//9000 (0x2328) Internet Explorer 9. Internet Explorer 9默认值，在IE9标准模式中按照网页上!DOCTYPE指令来显示网页。
            强制ie8,//8888 (0x22B8) Internet Explorer 8，强制IE8标准模式显示，忽略!DOCTYPE指令
            标准ie8,//8000 (0x1F40) Internet Explorer 8默认设置，在IE8标准模式中按照网页上!DOCTYPE指令展示网页
            标准ie7//7000 (0x1B58) 使用WebBrowser Control控件的应用程序所使用的默认值，在IE7标准模式中按照网页上!DOCTYPE指令来展示网页
        }

        /// <summary>
        /// 设置WebBrowser的默认版本
        /// </summary>
        /// <param name="ver">IE版本</param>
        private void SetIE(IeVersion ver)
        {
            string productName = AppDomain.CurrentDomain.SetupInformation.ApplicationName;//获取程序名称

            object version;
            switch (ver)
            {
                case IeVersion.标准ie7:
                    version = 0x1B58;
                    break;
                case IeVersion.标准ie8:
                    version = 0x1F40;
                    break;
                case IeVersion.强制ie8:
                    version = 0x22B8;
                    break;
                case IeVersion.标准ie9:
                    version = 0x2328;
                    break;
                case IeVersion.强制ie9:
                    version = 0x270F;
                    break;
                case IeVersion.标准ie10:
                    version = 0x02710;
                    break;
                case IeVersion.强制ie10:
                    version = 0x2711;
                    break;
                default:
                    version = 0x1F40;
                    break;
            }

            RegistryKey key = Registry.CurrentUser;
            RegistryKey software =
                key.CreateSubKey(
                    @"Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION\" + productName);
            if (software != null)
            {
                software.Close();
                software.Dispose();
            }
            RegistryKey wwui =
                key.OpenSubKey(
                    @"Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);
            //该项必须已存在
            if (wwui != null) wwui.SetValue(productName, version, RegistryValueKind.DWord);
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}
