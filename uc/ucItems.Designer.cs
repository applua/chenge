﻿
namespace Change.uc
{
    partial class ucItems
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.twItems = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabAttr = new System.Windows.Forms.TabControl();
            this.tabIntroduce = new System.Windows.Forms.TabPage();
            this.tabAttribute = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.webAttr = new System.Windows.Forms.WebBrowser();
            this.webInstroduce = new System.Windows.Forms.WebBrowser();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabAttr.SuspendLayout();
            this.tabIntroduce.SuspendLayout();
            this.tabAttribute.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.twItems);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(269, 599);
            this.panel1.TabIndex = 0;
            // 
            // twItems
            // 
            this.twItems.Location = new System.Drawing.Point(0, 3);
            this.twItems.Name = "twItems";
            this.twItems.Size = new System.Drawing.Size(264, 593);
            this.twItems.TabIndex = 0;
            this.twItems.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.twItems_NodeMouseDoubleClick_1);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabAttr);
            this.panel2.Location = new System.Drawing.Point(271, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(805, 597);
            this.panel2.TabIndex = 1;
            // 
            // tabAttr
            // 
            this.tabAttr.Controls.Add(this.tabIntroduce);
            this.tabAttr.Controls.Add(this.tabAttribute);
            this.tabAttr.Controls.Add(this.tabPage3);
            this.tabAttr.Controls.Add(this.tabPage1);
            this.tabAttr.Controls.Add(this.tabPage2);
            this.tabAttr.Location = new System.Drawing.Point(0, 3);
            this.tabAttr.Name = "tabAttr";
            this.tabAttr.SelectedIndex = 0;
            this.tabAttr.Size = new System.Drawing.Size(802, 595);
            this.tabAttr.TabIndex = 0;
            // 
            // tabIntroduce
            // 
            this.tabIntroduce.Controls.Add(this.webInstroduce);
            this.tabIntroduce.Location = new System.Drawing.Point(4, 22);
            this.tabIntroduce.Name = "tabIntroduce";
            this.tabIntroduce.Padding = new System.Windows.Forms.Padding(3);
            this.tabIntroduce.Size = new System.Drawing.Size(794, 569);
            this.tabIntroduce.TabIndex = 0;
            this.tabIntroduce.Text = "简介";
            this.tabIntroduce.UseVisualStyleBackColor = true;
            // 
            // tabAttribute
            // 
            this.tabAttribute.Controls.Add(this.webAttr);
            this.tabAttribute.Location = new System.Drawing.Point(4, 22);
            this.tabAttribute.Name = "tabAttribute";
            this.tabAttribute.Padding = new System.Windows.Forms.Padding(3);
            this.tabAttribute.Size = new System.Drawing.Size(794, 569);
            this.tabAttribute.TabIndex = 1;
            this.tabAttribute.Text = "属性";
            this.tabAttribute.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(794, 569);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "组成材料";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(794, 569);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "回收材料";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.richTextBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(794, 569);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "技能需求";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(5, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(788, 565);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // webAttr
            // 
            this.webAttr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webAttr.Location = new System.Drawing.Point(3, 3);
            this.webAttr.MinimumSize = new System.Drawing.Size(20, 20);
            this.webAttr.Name = "webAttr";
            this.webAttr.Size = new System.Drawing.Size(788, 563);
            this.webAttr.TabIndex = 0;
            // 
            // webInstroduce
            // 
            this.webInstroduce.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webInstroduce.Location = new System.Drawing.Point(3, 3);
            this.webInstroduce.MinimumSize = new System.Drawing.Size(20, 20);
            this.webInstroduce.Name = "webInstroduce";
            this.webInstroduce.Size = new System.Drawing.Size(788, 563);
            this.webInstroduce.TabIndex = 0;
            // 
            // ucItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucItems";
            this.Size = new System.Drawing.Size(1077, 625);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabAttr.ResumeLayout(false);
            this.tabIntroduce.ResumeLayout(false);
            this.tabAttribute.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabAttr;
        private System.Windows.Forms.TabPage tabAttribute;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TreeView twItems;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TabPage tabIntroduce;
        private System.Windows.Forms.WebBrowser webAttr;
        private System.Windows.Forms.WebBrowser webInstroduce;
    }
}
