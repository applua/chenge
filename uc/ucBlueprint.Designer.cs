﻿
namespace Change.uc
{
    partial class ucBlueprint
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tvBluePrint = new System.Windows.Forms.TreeView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labProcess = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.labprofit = new System.Windows.Forms.Label();
            this.labProduct = new System.Windows.Forms.Label();
            this.labMatir = new System.Windows.Forms.Label();
            this.cmbsell = new System.Windows.Forms.ComboBox();
            this.cmbbuy = new System.Windows.Forms.ComboBox();
            this.tbProductNum = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbProduct = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.labinvention = new System.Windows.Forms.Label();
            this.labmanufacturing = new System.Windows.Forms.Label();
            this.labresearch_material = new System.Windows.Forms.Label();
            this.labresearch_time = new System.Windows.Forms.Label();
            this.labCopytime = new System.Windows.Forms.Label();
            this.tbmax = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbprocess = new System.Windows.Forms.TextBox();
            this.cbTime = new System.Windows.Forms.ComboBox();
            this.cbMaterial = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.labpft = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dwMaterials = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbProduct = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.dwInvMaterials = new System.Windows.Forms.DataGridView();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cbData2 = new System.Windows.Forms.ComboBox();
            this.cbData1 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.cbRace = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tbInvRate = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbInvNum = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbInv = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dwMaterials)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dwInvMaterials)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.tvBluePrint);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 610);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(254, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(817, 620);
            this.panel2.TabIndex = 1;
            // 
            // tvBluePrint
            // 
            this.tvBluePrint.Location = new System.Drawing.Point(0, 0);
            this.tvBluePrint.Name = "tvBluePrint";
            this.tvBluePrint.Size = new System.Drawing.Size(248, 607);
            this.tvBluePrint.TabIndex = 0;
            this.tvBluePrint.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.tvBluePrint_NodeMouseDoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(256, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(960, 606);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(952, 580);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "蓝图制造";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labProcess);
            this.panel3.Controls.Add(this.label29);
            this.panel3.Controls.Add(this.labprofit);
            this.panel3.Controls.Add(this.labProduct);
            this.panel3.Controls.Add(this.labMatir);
            this.panel3.Controls.Add(this.cmbsell);
            this.panel3.Controls.Add(this.cmbbuy);
            this.panel3.Controls.Add(this.tbProductNum);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.tbProduct);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.labinvention);
            this.panel3.Controls.Add(this.labmanufacturing);
            this.panel3.Controls.Add(this.labresearch_material);
            this.panel3.Controls.Add(this.labresearch_time);
            this.panel3.Controls.Add(this.labCopytime);
            this.panel3.Controls.Add(this.tbmax);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.tbprocess);
            this.panel3.Controls.Add(this.cbTime);
            this.panel3.Controls.Add(this.cbMaterial);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.labName);
            this.panel3.Controls.Add(this.labpft);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(949, 571);
            this.panel3.TabIndex = 0;
            // 
            // labProcess
            // 
            this.labProcess.AutoSize = true;
            this.labProcess.Location = new System.Drawing.Point(481, 199);
            this.labProcess.Name = "labProcess";
            this.labProcess.Size = new System.Drawing.Size(11, 12);
            this.labProcess.TabIndex = 35;
            this.labProcess.Text = "0";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(376, 199);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(77, 12);
            this.label29.TabIndex = 34;
            this.label29.Text = "生产过程成本";
            // 
            // labprofit
            // 
            this.labprofit.AutoSize = true;
            this.labprofit.Location = new System.Drawing.Point(481, 225);
            this.labprofit.Name = "labprofit";
            this.labprofit.Size = new System.Drawing.Size(11, 12);
            this.labprofit.TabIndex = 33;
            this.labprofit.Text = "0";
            // 
            // labProduct
            // 
            this.labProduct.AutoSize = true;
            this.labProduct.Location = new System.Drawing.Point(268, 225);
            this.labProduct.Name = "labProduct";
            this.labProduct.Size = new System.Drawing.Size(11, 12);
            this.labProduct.TabIndex = 32;
            this.labProduct.Text = "0";
            // 
            // labMatir
            // 
            this.labMatir.AutoSize = true;
            this.labMatir.Location = new System.Drawing.Point(268, 199);
            this.labMatir.Name = "labMatir";
            this.labMatir.Size = new System.Drawing.Size(11, 12);
            this.labMatir.TabIndex = 31;
            this.labMatir.Text = "0";
            // 
            // cmbsell
            // 
            this.cmbsell.FormattingEnabled = true;
            this.cmbsell.Items.AddRange(new object[] {
            "市场均价",
            "买家最高价",
            "买家最低价",
            "卖家最低价",
            "卖家最高价"});
            this.cmbsell.Location = new System.Drawing.Point(27, 222);
            this.cmbsell.Name = "cmbsell";
            this.cmbsell.Size = new System.Drawing.Size(121, 20);
            this.cmbsell.TabIndex = 30;
            // 
            // cmbbuy
            // 
            this.cmbbuy.FormattingEnabled = true;
            this.cmbbuy.Items.AddRange(new object[] {
            "市场均价",
            "卖家最低价",
            "卖家最高价"});
            this.cmbbuy.Location = new System.Drawing.Point(27, 196);
            this.cmbbuy.Name = "cmbbuy";
            this.cmbbuy.Size = new System.Drawing.Size(121, 20);
            this.cmbbuy.TabIndex = 29;
            // 
            // tbProductNum
            // 
            this.tbProductNum.Location = new System.Drawing.Point(295, 161);
            this.tbProductNum.Name = "tbProductNum";
            this.tbProductNum.ReadOnly = true;
            this.tbProductNum.Size = new System.Drawing.Size(58, 21);
            this.tbProductNum.TabIndex = 28;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(248, 162);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 27;
            this.label15.Text = "数量：";
            // 
            // tbProduct
            // 
            this.tbProduct.Location = new System.Drawing.Point(96, 162);
            this.tbProduct.Name = "tbProduct";
            this.tbProduct.ReadOnly = true;
            this.tbProduct.Size = new System.Drawing.Size(146, 21);
            this.tbProduct.TabIndex = 26;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 164);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 25;
            this.label14.Text = "制造的产品：";
            // 
            // labinvention
            // 
            this.labinvention.AutoSize = true;
            this.labinvention.Location = new System.Drawing.Point(493, 162);
            this.labinvention.Name = "labinvention";
            this.labinvention.Size = new System.Drawing.Size(0, 12);
            this.labinvention.TabIndex = 24;
            // 
            // labmanufacturing
            // 
            this.labmanufacturing.AutoSize = true;
            this.labmanufacturing.Location = new System.Drawing.Point(493, 56);
            this.labmanufacturing.Name = "labmanufacturing";
            this.labmanufacturing.Size = new System.Drawing.Size(0, 12);
            this.labmanufacturing.TabIndex = 23;
            // 
            // labresearch_material
            // 
            this.labresearch_material.AutoSize = true;
            this.labresearch_material.Location = new System.Drawing.Point(493, 81);
            this.labresearch_material.Name = "labresearch_material";
            this.labresearch_material.Size = new System.Drawing.Size(0, 12);
            this.labresearch_material.TabIndex = 22;
            // 
            // labresearch_time
            // 
            this.labresearch_time.AutoSize = true;
            this.labresearch_time.Location = new System.Drawing.Point(493, 108);
            this.labresearch_time.Name = "labresearch_time";
            this.labresearch_time.Size = new System.Drawing.Size(0, 12);
            this.labresearch_time.TabIndex = 21;
            // 
            // labCopytime
            // 
            this.labCopytime.AutoSize = true;
            this.labCopytime.Location = new System.Drawing.Point(493, 136);
            this.labCopytime.Name = "labCopytime";
            this.labCopytime.Size = new System.Drawing.Size(0, 12);
            this.labCopytime.TabIndex = 20;
            // 
            // tbmax
            // 
            this.tbmax.Location = new System.Drawing.Point(257, 107);
            this.tbmax.Name = "tbmax";
            this.tbmax.ReadOnly = true;
            this.tbmax.Size = new System.Drawing.Size(53, 21);
            this.tbmax.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(186, 110);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 18;
            this.label13.Text = "最大流程数";
            // 
            // tbprocess
            // 
            this.tbprocess.Location = new System.Drawing.Point(109, 107);
            this.tbprocess.Name = "tbprocess";
            this.tbprocess.Size = new System.Drawing.Size(60, 21);
            this.tbprocess.TabIndex = 17;
            this.tbprocess.Text = "1";
            // 
            // cbTime
            // 
            this.cbTime.AutoCompleteCustomSource.AddRange(new string[] {
            "0%",
            "2%",
            "4%",
            "6%",
            "8%",
            "10%",
            "12%",
            "14%",
            "16%",
            "18%",
            "20%"});
            this.cbTime.FormattingEnabled = true;
            this.cbTime.Items.AddRange(new object[] {
            "0%",
            "2%",
            "4%",
            "6%",
            "8%",
            "10%",
            "12%",
            "14%",
            "16%",
            "18%",
            "20%"});
            this.cbTime.Location = new System.Drawing.Point(188, 56);
            this.cbTime.Name = "cbTime";
            this.cbTime.Size = new System.Drawing.Size(121, 20);
            this.cbTime.TabIndex = 15;
            this.cbTime.SelectedIndexChanged += new System.EventHandler(this.cbTime_SelectedIndexChanged);
            // 
            // cbMaterial
            // 
            this.cbMaterial.AutoCompleteCustomSource.AddRange(new string[] {
            "0%",
            "1%",
            "2%",
            "3%",
            "4%",
            "5%",
            "6%",
            "7%",
            "8%",
            "9%",
            "10%"});
            this.cbMaterial.FormattingEnabled = true;
            this.cbMaterial.Items.AddRange(new object[] {
            "0%",
            "1%",
            "2%",
            "3%",
            "4%",
            "5%",
            "6%",
            "7%",
            "8%",
            "9%",
            "10%"});
            this.cbMaterial.Location = new System.Drawing.Point(29, 56);
            this.cbMaterial.Name = "cbMaterial";
            this.cbMaterial.Size = new System.Drawing.Size(121, 20);
            this.cbMaterial.TabIndex = 14;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(376, 164);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 13;
            this.label12.Text = "发明时间";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(376, 137);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "拷贝时间";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(376, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 11;
            this.label10.Text = "研究时间效率";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(376, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 10;
            this.label9.Text = "研究材料效率";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(374, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 9;
            this.label8.Text = "制造时间：";
            // 
            // labName
            // 
            this.labName.AutoSize = true;
            this.labName.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labName.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.labName.Location = new System.Drawing.Point(491, 19);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(94, 21);
            this.labName.TabIndex = 8;
            this.labName.Text = "蓝图名称";
            // 
            // labpft
            // 
            this.labpft.AutoSize = true;
            this.labpft.Location = new System.Drawing.Point(376, 225);
            this.labpft.Name = "labpft";
            this.labpft.Size = new System.Drawing.Size(53, 12);
            this.labpft.TabIndex = 7;
            this.labpft.Text = "产品利润";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 225);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "产品市场价格";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(154, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "生产材料成本";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "生产：流程数";
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.Controls.Add(this.dwMaterials);
            this.panel4.Location = new System.Drawing.Point(4, 244);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(942, 326);
            this.panel4.TabIndex = 3;
            // 
            // dwMaterials
            // 
            this.dwMaterials.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dwMaterials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dwMaterials.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11});
            this.dwMaterials.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dwMaterials.Location = new System.Drawing.Point(0, 0);
            this.dwMaterials.Name = "dwMaterials";
            this.dwMaterials.ReadOnly = true;
            this.dwMaterials.RowTemplate.Height = 23;
            this.dwMaterials.Size = new System.Drawing.Size(942, 326);
            this.dwMaterials.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "物品类别";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "物品名称";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "最佳数量";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "加成数量";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "标准数量";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "价格";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "资金小计";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column8.HeaderText = "占用成本%";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "材料损耗%";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // Column10
            // 
            this.Column10.HeaderText = "浪费材料";
            this.Column10.Name = "Column10";
            this.Column10.ReadOnly = true;
            // 
            // Column11
            // 
            this.Column11.HeaderText = "体积";
            this.Column11.Name = "Column11";
            this.Column11.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(186, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "时间效率";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "材料效率";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.cmbProduct);
            this.tabPage2.Controls.Add(this.panel5);
            this.tabPage2.Controls.Add(this.radioButton2);
            this.tabPage2.Controls.Add(this.radioButton1);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.comboBox11);
            this.tabPage2.Controls.Add(this.comboBox10);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.cbData2);
            this.tabPage2.Controls.Add(this.cbData1);
            this.tabPage2.Controls.Add(this.comboBox4);
            this.tabPage2.Controls.Add(this.cbRace);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.tbInvRate);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.tbInvNum);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.tbInv);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(952, 580);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "蓝图发明";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(4, 26);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 57;
            this.label26.Text = "当前发明蓝图";
            // 
            // cmbProduct
            // 
            this.cmbProduct.FormattingEnabled = true;
            this.cmbProduct.Location = new System.Drawing.Point(109, 125);
            this.cmbProduct.Name = "cmbProduct";
            this.cmbProduct.Size = new System.Drawing.Size(121, 20);
            this.cmbProduct.TabIndex = 56;
            this.cmbProduct.SelectedIndexChanged += new System.EventHandler(this.cmbProduct_SelectedIndexChanged);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.dwInvMaterials);
            this.panel5.Location = new System.Drawing.Point(2, 255);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(949, 324);
            this.panel5.TabIndex = 55;
            // 
            // dwInvMaterials
            // 
            this.dwInvMaterials.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dwInvMaterials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dwInvMaterials.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16});
            this.dwInvMaterials.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dwInvMaterials.Location = new System.Drawing.Point(0, 0);
            this.dwInvMaterials.Name = "dwInvMaterials";
            this.dwInvMaterials.RowTemplate.Height = 23;
            this.dwInvMaterials.Size = new System.Drawing.Size(949, 324);
            this.dwInvMaterials.TabIndex = 0;
            // 
            // Column12
            // 
            this.Column12.HeaderText = "材料类别";
            this.Column12.Name = "Column12";
            // 
            // Column13
            // 
            this.Column13.HeaderText = "材料名称";
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            this.Column14.HeaderText = "材料数量";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.HeaderText = "材料单价";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.HeaderText = "价格小计";
            this.Column16.Name = "Column16";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(174, 159);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(59, 16);
            this.radioButton2.TabIndex = 54;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "单流程";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(109, 159);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(59, 16);
            this.radioButton1.TabIndex = 53;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "满流程";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(26, 164);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 12);
            this.label25.TabIndex = 52;
            this.label25.Text = "蓝图流程";
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(405, 156);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(121, 20);
            this.comboBox11.TabIndex = 51;
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(405, 120);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(121, 20);
            this.comboBox10.TabIndex = 50;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(334, 159);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 12);
            this.label24.TabIndex = 49;
            this.label24.Text = "材料价格";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(334, 128);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 12);
            this.label23.TabIndex = 48;
            this.label23.Text = "成品价格";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 128);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(89, 12);
            this.label22.TabIndex = 46;
            this.label22.Text = "我要发明的蓝图";
            // 
            // cbData2
            // 
            this.cbData2.FormattingEnabled = true;
            this.cbData2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cbData2.Location = new System.Drawing.Point(405, 91);
            this.cbData2.Name = "cbData2";
            this.cbData2.Size = new System.Drawing.Size(121, 20);
            this.cbData2.TabIndex = 45;
            // 
            // cbData1
            // 
            this.cbData1.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cbData1.FormattingEnabled = true;
            this.cbData1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cbData1.Location = new System.Drawing.Point(405, 63);
            this.cbData1.Name = "cbData1";
            this.cbData1.Size = new System.Drawing.Size(123, 20);
            this.cbData1.TabIndex = 44;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(109, 94);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 20);
            this.comboBox4.TabIndex = 43;
            // 
            // cbRace
            // 
            this.cbRace.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cbRace.FormattingEnabled = true;
            this.cbRace.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cbRace.Location = new System.Drawing.Point(109, 63);
            this.cbRace.Name = "cbRace";
            this.cbRace.Size = new System.Drawing.Size(121, 20);
            this.cbRace.TabIndex = 42;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(282, 94);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(107, 12);
            this.label21.TabIndex = 41;
            this.label21.Text = "数据核心2技能等级";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(280, 66);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 12);
            this.label20.TabIndex = 40;
            this.label20.Text = "数据核心1技能等级";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(26, 94);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 12);
            this.label19.TabIndex = 39;
            this.label19.Text = "祭品与衍生品";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(26, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(77, 12);
            this.label18.TabIndex = 38;
            this.label18.Text = "种族加密等级";
            // 
            // tbInvRate
            // 
            this.tbInvRate.Location = new System.Drawing.Point(435, 24);
            this.tbInvRate.Name = "tbInvRate";
            this.tbInvRate.ReadOnly = true;
            this.tbInvRate.Size = new System.Drawing.Size(58, 21);
            this.tbInvRate.TabIndex = 36;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(376, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 12);
            this.label17.TabIndex = 34;
            this.label17.Text = "成功率：";
            // 
            // tbInvNum
            // 
            this.tbInvNum.Location = new System.Drawing.Point(294, 23);
            this.tbInvNum.Name = "tbInvNum";
            this.tbInvNum.ReadOnly = true;
            this.tbInvNum.Size = new System.Drawing.Size(58, 21);
            this.tbInvNum.TabIndex = 37;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(247, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 12);
            this.label16.TabIndex = 35;
            this.label16.Text = "数量：";
            // 
            // tbInv
            // 
            this.tbInv.Location = new System.Drawing.Point(87, 21);
            this.tbInv.Name = "tbInv";
            this.tbInv.ReadOnly = true;
            this.tbInv.Size = new System.Drawing.Size(146, 21);
            this.tbInv.TabIndex = 33;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(952, 580);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "蓝图参数设置";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(952, 580);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "蓝图收藏";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(952, 580);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "蓝图材料";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(952, 580);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "蓝图利润排行";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // ucBlueprint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucBlueprint";
            this.Size = new System.Drawing.Size(1219, 624);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dwMaterials)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dwInvMaterials)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView tvBluePrint;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labpft;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dwMaterials;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ComboBox cbTime;
        private System.Windows.Forms.ComboBox cbMaterial;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbmax;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbprocess;
        private System.Windows.Forms.Label labCopytime;
        private System.Windows.Forms.Label labinvention;
        private System.Windows.Forms.Label labmanufacturing;
        private System.Windows.Forms.Label labresearch_material;
        private System.Windows.Forms.Label labresearch_time;
        private System.Windows.Forms.TextBox tbProductNum;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbProduct;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbInvRate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox tbInvNum;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cmbsell;
        private System.Windows.Forms.ComboBox cmbbuy;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox cbData2;
        private System.Windows.Forms.ComboBox cbData1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox cbRace;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbInv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.DataGridView dwInvMaterials;
        private System.Windows.Forms.ComboBox cmbProduct;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.Label labprofit;
        private System.Windows.Forms.Label labProduct;
        private System.Windows.Forms.Label labMatir;
        private System.Windows.Forms.Label labProcess;
        private System.Windows.Forms.Label label29;
    }
}
