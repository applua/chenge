﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

namespace Change
{
    public partial class ucPrice : UserControl
    {
        SQLiteConnection conn;
        //定义委托
        delegate void inittvItemsDelegate();
        delegate void AsynUpdateUI1(string step);   //更新UI1
        delegate void AsynUpdateUI2(TreeNode root);//更新UI2

        public delegate void UpdateUI(string step);//声明一个更新主线程的委托
        public UpdateUI UpdateUIDelegate;

        public delegate void AccomplishTask(TreeNode root);//声明一个在完成任务时通知主线程的委托
        public AccomplishTask TaskCallBack;

        string yearID;
        // tqapi:宁静服   infapt:曙光服
        private string api = "tqapi";
        int thflag = 0;   //线程完成标识

        public ucPrice()
        {
            InitializeComponent();

            conn = new SQLiteConnection("Data Source=db/eve.db;Version=3;");
            conn.Open();

            cbStarfieldInit();
            cbCnInit();
            cbGalaxyInit();

            twItems.Nodes.Add("正在连接数据库");
            yearID = getYearID();

            UpdateUIDelegate += UpdataUIStatus;//绑定更新任务状态的委托
            TaskCallBack += Accomplish;//绑定完成任务要调用的委托
            //启动线程
            Thread thread = new Thread(intitvItems);
            thread.IsBackground = true;
            thread.Start();
            //intitvItems();

            
        }

        private void cbStarfieldInit()
        {
            string sql = "select * from starField";

            using (SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn))
            {
                DataTable dt = new DataTable();
                reader.Fill(dt);//填充数据集，即获取数据集
                this.cbStarfield.DataSource = dt.DefaultView; //设置ComboBox的数据源
                this.cbStarfield.DisplayMember = "sName"; //让ComboBox显示renterName列
                this.cbStarfield.ValueMember = "starFieldID";  //让ComboBox实际的值为renterId列

                this.cbStarfield.SelectedIndex = 1;

            }
        }

        private void cbCnInit()
        {

            string sql = "select * from constellation where starFieldID=" + this.cbStarfield.SelectedValue;

            using (SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn))
            {
                DataTable dt = new DataTable();
                reader.Fill(dt);//填充数据集，即获取数据集
                this.cbCn.DataSource = dt.DefaultView; //设置ComboBox的数据源
                this.cbCn.DisplayMember = "cName"; //让ComboBox显示renterName列
                this.cbCn.ValueMember = "constellationID";  //让ComboBox实际的值为renterId列

                this.cbCn.SelectedIndex=3;

            }
        }
        private void cbGalaxyInit()
        {
            string sql = "select * from galaxy where  starFieldID="+this.cbStarfield.SelectedValue+ " and constellationID="
                + this.cbCn.SelectedValue;

            using (SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn))
            {
                DataTable dt = new DataTable();
                reader.Fill(dt);//填充数据集，即获取数据集
                this.cbGalaxy.DataSource = dt.DefaultView; //设置ComboBox的数据源
                this.cbGalaxy.DisplayMember = "gName"; //让ComboBox显示renterName列
                this.cbGalaxy.ValueMember = "galaxyID";  //让ComboBox实际的值为renterId列

                this.cbGalaxy.SelectedIndex = 3;

            }
        }
        //更新UI
        private void UpdataUIStatus(string step)
        {
            if (InvokeRequired)
            {
                this.Invoke(new AsynUpdateUI1(delegate (string s)
                {
                    //this.pgbWrite.Value += s;
                    //this.lblWriteStatus.Text = this.pgbWrite.Value.ToString() + "/" + this.pgbWrite.Maximum.ToString();
                    twItems.Nodes.Add(s);
                }), step);
            }
        }

        //完成任务时需要调用
        private void Accomplish(TreeNode root)
        {
            if (InvokeRequired)
            {
                this.Invoke(new AsynUpdateUI2(delegate (TreeNode rt)
                {
                    //this.pgbWrite.Value += s;
                    //this.lblWriteStatus.Text = this.pgbWrite.Value.ToString() + "/" + this.pgbWrite.Maximum.ToString();
                    twItems.Nodes.Clear();
                    twItems.Nodes.Add(rt);
                    this.cbCn.Enabled= true;
                    this.cbStarfield.Enabled= true;
                }), root);
                this.thflag = 1;           
            }
        }
        private void ucPrice_Load(object sender, System.EventArgs e)
        {
            
        }
        private void  intitvItems()
        {
            TreeNode root = new TreeNode();

 
            root.Tag = 0;
            root.Text = "物品数据库-"+ yearID;
            root.Expand();
            //   twItems.Nodes.Add(root);
          try
          {


            UpdateUIDelegate("数据库已连接，正在加载数据...");
          }
          catch(Exception ex)
            {
                throw ex;
            }
               

            setTreeView(root, 0);
            UpdateUIDelegate("市场组数据加载完毕，正在加载物品数据");
            setTreeViewLeaf(root);
            UpdateUIDelegate("物品组数据加载完毕.");
            TaskCallBack(root);
            
        }

        private string getYearID()
        {
            FileStream fs = new FileStream("db/eve.cfg", FileMode.Open, FileAccess.Read);

            StreamReader sr = new StreamReader(fs);

            string yearid = sr.ReadToEnd();

            return yearid;

            throw new NotImplementedException();
        }

        /// <summary>
        /// 查找叶子节点
        /// </summary>
        /// <param name="tr1"></param>
        private void setTreeViewLeaf(TreeNode tr1)
        {
           
            foreach(TreeNode node in tr1.Nodes)
            {
                //查找树的叶子节点
                if(node.Nodes.Count==0)
                {
                    addNodeLeaf(node);
                     
                }
                else
                {
                    findTreeViewLeaf(node);
                }
                UpdateUIDelegate("加载物品组"+node.Text);
            }  
        }
        /// <summary>
        /// 第二层遍历，加速函数
        /// </summary>
        /// <param name="tr1"></param>
        private void findTreeViewLeaf(TreeNode tr1)
        {
            foreach (TreeNode node in tr1.Nodes)
            {
                //查找树的叶子节点
                if (node.Nodes.Count == 0)
                {
                    addNodeLeaf(node);

                }
                else
                {
                    flashTreeViewLeaf(node);
                }

            }
        }
        /// <summary>
        /// 第三层遍历，加速函数
        /// </summary>
        /// <param name="node"></param>
        private void flashTreeViewLeaf(TreeNode tr1)
        {
            foreach (TreeNode node in tr1.Nodes)
            {
                //查找树的叶子节点
                if (node.Nodes.Count == 0)
                {
                    addNodeLeaf(node);

                }
                else
                {
                    flashTreeViewLeaf(node);
                }

            }
        }


        /// <summary>
        /// 添加叶子节点
        /// </summary>
        /// <param name="node"></param>
        private void addNodeLeaf(TreeNode node)
        {
            int pid = Convert.ToInt32(node.Tag);

            string sql = "select typeID,typeName_zh from invtypes where marketGroupID="+pid;

            SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn);
             
            DataTable dtLeaf = new DataTable();

            reader.Fill(dtLeaf);
            if (dtLeaf.Rows.Count > 0)
            {
                foreach(DataRow dr in dtLeaf.Rows)
                {
                    TreeNode nodeleaf = new TreeNode();
                    nodeleaf.Text = dr[1].ToString();
                    nodeleaf.Tag = Convert.ToInt32(dr[0]);
                
                    node.Nodes.Add(nodeleaf);
                }
            }
        }

        private void setTreeView(TreeNode tr1, int parentId)
        {
            string sql;
            if (parentId == 0)
                sql = "select marketGroupID,marketGroupName_zh,parentGroupID from invmarketgroups where parentGroupID is null";
            else
                sql = "select marketGroupID,marketGroupName_zh,parentGroupID from invmarketgroups where parentGroupID=" + parentId;
            SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn);

            DataTable dt = new DataTable();
            reader.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                
                int pId = -1;
                  foreach (DataRow row in dt.Rows)
                  {
                         TreeNode node = new TreeNode();
                         node.Text = row[1].ToString();                     
                         node.Tag =  Convert.ToInt32(row[0]);
                         if (row[2] is System.DBNull)
                            pId = 0;
                         else
                                pId = Convert.ToInt32(row[2]);
                         if (pId == 0)
                          {
                              //添加根节点
                              tr1.Nodes.Add(node);
                          }
                          else
                          {
                              //添加根节点之外的其他节点
                              RefreshChildNode(tr1, node, pId);
                          }
                          //查找以node为父节点的子节点
                          setTreeView(tr1, Convert.ToInt32(node.Tag));
 
                  }
             }
 
         }
        //处理根节点的子节点
        private void RefreshChildNode(TreeNode tr1, TreeNode treeNode, int parentId)
        {
                    foreach (TreeNode node in tr1.Nodes)
                            {
                                if (Convert.ToInt32(node.Tag) == parentId)
                                    {
                                        node.Nodes.Add(treeNode);
                                        return;
                                    }else if (node.Nodes.Count > 0)
                                    {
                                        FindChildNode(node, treeNode, parentId);
                                    }
                            }
            }
 
        //处理根节点的子节点的子节点
        private void FindChildNode(TreeNode tNode, TreeNode treeNode, int parentId)
       {
                   foreach (TreeNode node in tNode.Nodes)
                        {
                              if (Convert.ToInt32(node.Tag) == parentId)
                                 {
                                      node.Nodes.Add(treeNode);
                                     return;
                                 }else if (node.Nodes.Count > 0)
                                 {
                                      FindChildNode(node, treeNode, parentId);
                                  }
       
                     }
   
        }

        private void btConfirm_Click(object sender, EventArgs e)
        {
            
        }


        private void twItems_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            string buyitem, sellitem;
            string retString = null;
           
            int typeID = Convert.ToInt32(e.Node.Tag);
            if (e.Node.Nodes.Count > 0)
                return;
            


            string url = "http://www.ceve-market.org/"+api+"/market/region/"+this.cbStarfield.SelectedValue+ "/system/"+this.cbGalaxy.SelectedValue+"/type/" + typeID + ".json";   //国服市场中心API的URL
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Proxy = null;
                request.KeepAlive = false;
                request.Method = "GET";
                request.ContentType = "application/json; charset=UTF-8";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream rs = response.GetResponseStream();
                StreamReader sr = new StreamReader(rs, Encoding.UTF8);
                retString = sr.ReadToEnd();
                rs.Close();
                sr.Close();
            }catch(WebException ex)
            {
                MessageBox.Show("错误："+ex);
                throw ex;
            }


            JObject price = JObject.Parse(retString);
            JObject sell = (JObject)price["sell"];
            JObject buy = (JObject)price["buy"];
            string sel = (string)sell["min"];
            string bu = (string)buy["max"]; //JSON转化
            //long lsell = (long)sell["volume"];
            //DateTime dtsell = new DateTime(lsell);
            //long lbuy = (long)buy["volume"];
            //DateTime dtbuy= new DateTime(lbuy);

            if (sel==null)
            {
                sellitem = " ";
            }
            else
            {
                  
                sellitem = string.Format("{0:0,0.00}",float.Parse(sel));
            }
            if (bu == null)
            {
                buyitem = " ";
            }
            else
            {
                buyitem = string.Format("{0:0,0.00}", float.Parse(bu));
            }
            int index = this.dgbPrice.Rows.Add();
            dgbPrice.Rows[index].Cells[0].Value = 1+index;
            dgbPrice.Rows[index].Cells[1].Value = typeID;
            dgbPrice.Rows[index].Cells[2].Value = e.Node.Text;
            dgbPrice.Rows[index].Cells[3].Value = sellitem;
            //dgbPrice.Rows[index].Cells[3].Value = dtsell.ToString("f");
            dgbPrice.Rows[index].Cells[4].Value = buyitem;
            //dgbPrice.Rows[index].Cells[5].Value = dtbuy.ToString("f");
            //
            tabPrice.SelectedTab = tabPrice.TabPages[0];
        }

        private void dgbPrice_ControlRemoved(object sender, ControlEventArgs e)
        {

        }

        private void dgbPrice_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Stream srString;
            int index;
            int typeID = Convert.ToInt32(dgbPrice.Rows[e.RowIndex].Cells[1].Value);
            

            string url = " https://www.ceve-market.org/"+api+"/quicklook?typeid=" + typeID + "&regionlimit=10000002&setminQ=10000";
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Proxy = null;
                request.KeepAlive = false;
                request.Method = "GET";
                request.ContentType = "application/json; charset=UTF-8";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                srString = response.GetResponseStream();

            }
            catch (WebException ex)
            {
                MessageBox.Show("错误：" + ex);
                throw ex;
            }
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.Load(srString);

            dgvSell.Rows.Clear();
            dgvBuy.Rows.Clear();

            //指定一个节点
            XmlNode root = xmldoc.SelectSingleNode("//sell_orders");
            foreach (XmlNode node in root.ChildNodes)
            {

                index = this.dgvSell.Rows.Add();
                dgvSell.Rows[index].Cells[0].Value = 1 + index;
                dgvSell.Rows[index].Cells[1].Value = node.ChildNodes[2].InnerText;
                dgvSell.Rows[index].Cells[2].Value = node.ChildNodes[5].InnerText;
                dgvSell.Rows[index].Cells[3].Value = node.ChildNodes[6].InnerText;
                dgvSell.Rows[index].Cells[4].Value = node.ChildNodes[7].InnerText;
                dgvSell.Rows[index].Cells[5].Value = node.ChildNodes[8].InnerText;
                dgvSell.Rows[index].Cells[6].Value = node.ChildNodes[9].InnerText;

            }
            //指定一个节点
            root = xmldoc.SelectSingleNode("//buy_orders");
            foreach (XmlNode node in root.ChildNodes)
            {
                index = this.dgvBuy.Rows.Add();
                dgvBuy.Rows[index].Cells[0].Value = 1 + index;
                dgvBuy.Rows[index].Cells[1].Value = node.ChildNodes[2].InnerText;
                dgvBuy.Rows[index].Cells[2].Value = node.ChildNodes[5].InnerText;
                dgvBuy.Rows[index].Cells[3].Value = node.ChildNodes[6].InnerText;
                dgvBuy.Rows[index].Cells[4].Value = node.ChildNodes[7].InnerText;
                dgvBuy.Rows[index].Cells[5].Value = node.ChildNodes[8].InnerText;
                dgvBuy.Rows[index].Cells[6].Value = node.ChildNodes[9].InnerText;
            }

            tabPrice.SelectedTab = tabPrice.TabPages[1];
        }

        private void rbInf_CheckedChanged(object sender, EventArgs e)
        {
            api = "infapi";
        }

        private void rbTq_CheckedChanged(object sender, EventArgs e)
        {
            api = "tqapi";
        }

        private void cbStarfield_SelectedIndexChanged(object sender, EventArgs e)
        {
             if(this.thflag==1)
            {
                cbCnChange(this.cbStarfield.SelectedValue);
                cbGalaxyChange(this.cbCn.SelectedValue);
            }

        }

        private void cbGalaxyChange(object selectedValue)
        {
            string sql = "select * from galaxy where  starFieldID=" + this.cbStarfield.SelectedValue + " and constellationID="
    + this.cbCn.SelectedValue;

            using (SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn))
            {
                DataTable dt = new DataTable();
                reader.Fill(dt);//填充数据集，即获取数据集
                this.cbGalaxy.DataSource = dt.DefaultView; //设置ComboBox的数据源
                this.cbGalaxy.DisplayMember = "gName"; //让ComboBox显示renterName列
                this.cbGalaxy.ValueMember = "galaxyID";  //让ComboBox实际的值为renterId列

                this.cbGalaxy.SelectedIndex = 0;

            }
        }

        private void cbCnChange(object selectedValue)
        {
            string sql = "select * from constellation where starFieldID=" + this.cbStarfield.SelectedValue;

            using (SQLiteDataAdapter reader = new SQLiteDataAdapter(sql, conn))
            {
                DataTable dt = new DataTable();
                reader.Fill(dt);//填充数据集，即获取数据集
                this.cbCn.DataSource = dt.DefaultView; //设置ComboBox的数据源
                this.cbCn.DisplayMember = "cName"; //让ComboBox显示renterName列
                this.cbCn.ValueMember = "constellationID";  //让ComboBox实际的值为renterId列

                this.cbCn.SelectedIndex = 0;

            }
        }

        private void cbCn_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.thflag == 1)
            {
                
                cbGalaxyChange(this.cbCn.SelectedValue);
            }
        }
    }
}
