﻿
namespace Change.uc
{
    partial class ucTask
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.webTask = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webTask
            // 
            this.webTask.AllowWebBrowserDrop = false;
            this.webTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webTask.Location = new System.Drawing.Point(0, 0);
            this.webTask.MinimumSize = new System.Drawing.Size(20, 20);
            this.webTask.Name = "webTask";
            this.webTask.ScriptErrorsSuppressed = true;
            this.webTask.Size = new System.Drawing.Size(1482, 802);
            this.webTask.TabIndex = 0;
            this.webTask.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webTask_DocumentCompleted);
            // 
            // ucTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.webTask);
            this.Name = "ucTask";
            this.Size = new System.Drawing.Size(1482, 802);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webTask;
    }
}
