﻿
namespace Change
{
    partial class ucPrice
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucPrice));
            this.twItems = new System.Windows.Forms.TreeView();
            this.panelleft = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbInf = new System.Windows.Forms.RadioButton();
            this.rbTq = new System.Windows.Forms.RadioButton();
            this.tabPrice = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgbPrice = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvBuy = new System.Windows.Forms.DataGridView();
            this.序号 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvSell = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btConfirm = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbGalaxy = new System.Windows.Forms.ComboBox();
            this.cbCn = new System.Windows.Forms.ComboBox();
            this.cbStarfield = new System.Windows.Forms.ComboBox();
            this.panelleft.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPrice.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgbPrice)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuy)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSell)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // twItems
            // 
            this.twItems.Dock = System.Windows.Forms.DockStyle.Left;
            this.twItems.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.twItems.Location = new System.Drawing.Point(0, 0);
            this.twItems.Margin = new System.Windows.Forms.Padding(2);
            this.twItems.Name = "twItems";
            this.twItems.Size = new System.Drawing.Size(271, 626);
            this.twItems.TabIndex = 0;
            this.twItems.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.twItems_NodeMouseDoubleClick);
            // 
            // panelleft
            // 
            this.panelleft.AutoScroll = true;
            this.panelleft.AutoSize = true;
            this.panelleft.Controls.Add(this.groupBox2);
            this.panelleft.Controls.Add(this.tabPrice);
            this.panelleft.Controls.Add(this.btConfirm);
            this.panelleft.Controls.Add(this.textBox1);
            this.panelleft.Controls.Add(this.label4);
            this.panelleft.Controls.Add(this.label3);
            this.panelleft.Controls.Add(this.label2);
            this.panelleft.Controls.Add(this.label1);
            this.panelleft.Controls.Add(this.cbGalaxy);
            this.panelleft.Controls.Add(this.cbCn);
            this.panelleft.Controls.Add(this.cbStarfield);
            this.panelleft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelleft.Location = new System.Drawing.Point(271, 0);
            this.panelleft.Margin = new System.Windows.Forms.Padding(2);
            this.panelleft.Name = "panelleft";
            this.panelleft.Size = new System.Drawing.Size(861, 626);
            this.panelleft.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Location = new System.Drawing.Point(437, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(417, 126);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "设置";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbInf);
            this.groupBox1.Controls.Add(this.rbTq);
            this.groupBox1.Location = new System.Drawing.Point(3, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 49);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "市场价格";
            // 
            // rbInf
            // 
            this.rbInf.AutoSize = true;
            this.rbInf.Location = new System.Drawing.Point(114, 18);
            this.rbInf.Name = "rbInf";
            this.rbInf.Size = new System.Drawing.Size(59, 16);
            this.rbInf.TabIndex = 23;
            this.rbInf.Text = "曙光服";
            this.rbInf.UseVisualStyleBackColor = true;
            this.rbInf.CheckedChanged += new System.EventHandler(this.rbInf_CheckedChanged);
            // 
            // rbTq
            // 
            this.rbTq.AutoSize = true;
            this.rbTq.Checked = true;
            this.rbTq.Location = new System.Drawing.Point(13, 18);
            this.rbTq.Name = "rbTq";
            this.rbTq.Size = new System.Drawing.Size(59, 16);
            this.rbTq.TabIndex = 22;
            this.rbTq.TabStop = true;
            this.rbTq.Text = "宁静服";
            this.rbTq.UseVisualStyleBackColor = true;
            this.rbTq.CheckedChanged += new System.EventHandler(this.rbTq_CheckedChanged);
            // 
            // tabPrice
            // 
            this.tabPrice.Controls.Add(this.tabPage1);
            this.tabPrice.Controls.Add(this.tabPage2);
            this.tabPrice.Controls.Add(this.tabPage3);
            this.tabPrice.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabPrice.Location = new System.Drawing.Point(0, 129);
            this.tabPrice.Margin = new System.Windows.Forms.Padding(2);
            this.tabPrice.Name = "tabPrice";
            this.tabPrice.SelectedIndex = 0;
            this.tabPrice.Size = new System.Drawing.Size(854, 519);
            this.tabPrice.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgbPrice);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(846, 493);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "价格资信息";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgbPrice
            // 
            this.dgbPrice.AllowUserToAddRows = false;
            this.dgbPrice.AllowUserToOrderColumns = true;
            this.dgbPrice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgbPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgbPrice.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column13,
            this.Column6,
            this.Column3,
            this.Column4});
            this.dgbPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgbPrice.Location = new System.Drawing.Point(2, 2);
            this.dgbPrice.Margin = new System.Windows.Forms.Padding(2);
            this.dgbPrice.MultiSelect = false;
            this.dgbPrice.Name = "dgbPrice";
            this.dgbPrice.ReadOnly = true;
            this.dgbPrice.RowHeadersWidth = 51;
            this.dgbPrice.RowTemplate.Height = 27;
            this.dgbPrice.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgbPrice.Size = new System.Drawing.Size(842, 489);
            this.dgbPrice.TabIndex = 0;
            this.dgbPrice.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgbPrice_CellMouseDoubleClick);
            this.dgbPrice.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.dgbPrice_ControlRemoved);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "序号";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "typeID";
            this.Column13.Name = "Column13";
            this.Column13.ReadOnly = true;
            this.Column13.Visible = false;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "物品名称";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "最低卖价";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "最高买价";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage2.Size = new System.Drawing.Size(846, 493);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "订单资料";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvBuy);
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Location = new System.Drawing.Point(3, 262);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(848, 230);
            this.panel2.TabIndex = 1;
            // 
            // dgvBuy
            // 
            this.dgvBuy.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBuy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBuy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.序号,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column12});
            this.dgvBuy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBuy.Location = new System.Drawing.Point(0, 25);
            this.dgvBuy.Name = "dgvBuy";
            this.dgvBuy.RowTemplate.Height = 23;
            this.dgvBuy.Size = new System.Drawing.Size(848, 205);
            this.dgvBuy.TabIndex = 1;
            // 
            // 序号
            // 
            this.序号.HeaderText = "序号";
            this.序号.Name = "序号";
            // 
            // Column14
            // 
            this.Column14.HeaderText = "空间站";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.HeaderText = "价格";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.HeaderText = "数量";
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            this.Column17.HeaderText = "最小数量";
            this.Column17.Name = "Column17";
            // 
            // Column18
            // 
            this.Column18.HeaderText = "开始时间";
            this.Column18.Name = "Column18";
            // 
            // Column12
            // 
            this.Column12.HeaderText = "结束时间";
            this.Column12.Name = "Column12";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(848, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(36, 22);
            this.toolStripButton2.Text = "买单";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvSell);
            this.panel1.Controls.Add(this.toolStrip2);
            this.panel1.Location = new System.Drawing.Point(3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(850, 256);
            this.panel1.TabIndex = 0;
            // 
            // dgvSell
            // 
            this.dgvSell.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSell.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSell.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column5,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11});
            this.dgvSell.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSell.Location = new System.Drawing.Point(0, 25);
            this.dgvSell.Name = "dgvSell";
            this.dgvSell.RowTemplate.Height = 23;
            this.dgvSell.Size = new System.Drawing.Size(850, 231);
            this.dgvSell.TabIndex = 1;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "序号";
            this.Column2.Name = "Column2";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "空间站";
            this.Column5.Name = "Column5";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "价格";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "数量";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "最小数量";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "开始时间";
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "结束时间";
            this.Column11.Name = "Column11";
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip2.Location = new System.Drawing.Point(0, 0);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(850, 25);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 22);
            this.toolStripButton1.Text = "卖单";
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage3.Size = new System.Drawing.Size(846, 493);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "设置";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btConfirm
            // 
            this.btConfirm.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btConfirm.Location = new System.Drawing.Point(349, 37);
            this.btConfirm.Margin = new System.Windows.Forms.Padding(2);
            this.btConfirm.Name = "btConfirm";
            this.btConfirm.Size = new System.Drawing.Size(56, 25);
            this.btConfirm.TabIndex = 20;
            this.btConfirm.Text = "提交";
            this.btConfirm.UseVisualStyleBackColor = true;
            this.btConfirm.Click += new System.EventHandler(this.btConfirm_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(127, 37);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(209, 21);
            this.textBox1.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(43, 41);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 16);
            this.label4.TabIndex = 18;
            this.label4.Text = "物品搜索";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(298, 74);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "-";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 74);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(43, 71);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 19);
            this.label1.TabIndex = 15;
            this.label1.Text = "星域";
            // 
            // cbGalaxy
            // 
            this.cbGalaxy.FormattingEnabled = true;
            this.cbGalaxy.Location = new System.Drawing.Point(314, 72);
            this.cbGalaxy.Margin = new System.Windows.Forms.Padding(2);
            this.cbGalaxy.Name = "cbGalaxy";
            this.cbGalaxy.Size = new System.Drawing.Size(92, 20);
            this.cbGalaxy.TabIndex = 14;
            // 
            // cbCn
            // 
            this.cbCn.Enabled = false;
            this.cbCn.FormattingEnabled = true;
            this.cbCn.Location = new System.Drawing.Point(203, 72);
            this.cbCn.Margin = new System.Windows.Forms.Padding(2);
            this.cbCn.Name = "cbCn";
            this.cbCn.Size = new System.Drawing.Size(92, 20);
            this.cbCn.TabIndex = 13;
            this.cbCn.SelectedIndexChanged += new System.EventHandler(this.cbCn_SelectedIndexChanged);
            // 
            // cbStarfield
            // 
            this.cbStarfield.Enabled = false;
            this.cbStarfield.FormattingEnabled = true;
            this.cbStarfield.Location = new System.Drawing.Point(100, 71);
            this.cbStarfield.Margin = new System.Windows.Forms.Padding(2);
            this.cbStarfield.Name = "cbStarfield";
            this.cbStarfield.Size = new System.Drawing.Size(92, 20);
            this.cbStarfield.TabIndex = 12;
            this.cbStarfield.SelectedIndexChanged += new System.EventHandler(this.cbStarfield_SelectedIndexChanged);
            // 
            // ucPrice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.panelleft);
            this.Controls.Add(this.twItems);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ucPrice";
            this.Size = new System.Drawing.Size(1132, 626);
            this.Load += new System.EventHandler(this.ucPrice_Load);
            this.panelleft.ResumeLayout(false);
            this.panelleft.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPrice.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgbPrice)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBuy)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSell)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView twItems;
        private System.Windows.Forms.Panel panelleft;
        private System.Windows.Forms.TabControl tabPrice;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btConfirm;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbGalaxy;
        private System.Windows.Forms.ComboBox cbCn;
        private System.Windows.Forms.ComboBox cbStarfield;
        private System.Windows.Forms.DataGridView dgbPrice;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.DataGridView dgvSell;
        private System.Windows.Forms.DataGridView dgvBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn 序号;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbInf;
        private System.Windows.Forms.RadioButton rbTq;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}
