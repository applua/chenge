﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Change.uc
{
    public partial class ucAbyss : UserControl
    {
        public ucAbyss()
        {
            InitializeComponent();
            webAbyss.ScriptErrorsSuppressed = true; //禁用错误脚本提示 
            webAbyss.IsWebBrowserContextMenuEnabled = false; //禁用右键菜单 
            webAbyss.WebBrowserShortcutsEnabled = false; //禁用快捷键 
            webAbyss.AllowWebBrowserDrop = false;//禁止拖拽
            webAbyss.ScrollBarsEnabled = true;//禁止滚动条
            webAbyss.NewWindow += new CancelEventHandler(webBrowser1_NewWindow);  //屏蔽弹出新IE窗口
            string url = "https://www.evebk.com/%E4%B8%89%E7%A5%9E%E8%A3%94";
            webAbyss.Navigate(url);
        }
        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            try
            {
                string url = webAbyss.Document.ActiveElement.GetAttribute("href");

                webAbyss.Url = new Uri(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
