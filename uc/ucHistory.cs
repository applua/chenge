﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Change
{
    public partial class ucHistory : UserControl
    {
        public ucHistory()
        {
            InitializeComponent();
            webHistory.ScriptErrorsSuppressed = true; //禁用错误脚本提示 
            webHistory.IsWebBrowserContextMenuEnabled = false; //禁用右键菜单 
            webHistory.WebBrowserShortcutsEnabled = false; //禁用快捷键 
            webHistory.AllowWebBrowserDrop = false;//禁止拖拽
            webHistory.ScrollBarsEnabled = true;//禁止滚动条
            webHistory.NewWindow += new CancelEventHandler(webBrowser1_NewWindow);  //屏蔽弹出新IE窗口
            string url = "http://evepc.baike.163.com/ziliao/62.html";
            webHistory.Navigate(url);
        }
        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
