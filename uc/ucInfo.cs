﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Change.uc
{
    public partial class ucInfo : UserControl
    {
        public ucInfo()
        {
            InitializeComponent();
            webInfo.ScriptErrorsSuppressed = true; //禁用错误脚本提示 
            webInfo.IsWebBrowserContextMenuEnabled = false; //禁用右键菜单 
            webInfo.WebBrowserShortcutsEnabled = false; //禁用快捷键 
            webInfo.AllowWebBrowserDrop = false;//禁止拖拽
            webInfo.ScrollBarsEnabled = true;//禁止滚动条
            webInfo.NewWindow += new CancelEventHandler(webBrowser1_NewWindow);  //屏蔽弹出新IE窗口
            string url = "https://www.evebk.com/Category:%E6%AD%BB%E4%BA%A1%E7%A9%BA%E9%97%B4";
            webInfo.Navigate(url);
        }
        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            try
            {
                string url = webInfo.Document.ActiveElement.GetAttribute("href");

                webInfo.Url = new Uri(url);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
