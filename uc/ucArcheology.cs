﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Change.uc
{
    public partial class ucArcheology : UserControl
    {
        public ucArcheology()
        {
            InitializeComponent();
            webArcheology.ScriptErrorsSuppressed = true; //禁用错误脚本提示 
            webArcheology.IsWebBrowserContextMenuEnabled = false; //禁用右键菜单 
            webArcheology.WebBrowserShortcutsEnabled = false; //禁用快捷键 
            webArcheology.AllowWebBrowserDrop = false;//禁止拖拽
            webArcheology.ScrollBarsEnabled = true;//禁止滚动条
            webArcheology.NewWindow += new CancelEventHandler(webBrowser1_NewWindow);  //屏蔽弹出新IE窗口
            string url = "https://www.evebk.com/%E6%8E%A2%E7%B4%A2";
            webArcheology.Navigate(url);
        }
        private void webBrowser1_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

    }
}
